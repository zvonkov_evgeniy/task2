package com.sysgears.statistics;

import java.util.HashMap;

/**
 * Saves progress of iteration in multi-thread FileSplitter.
 */
public class Statistic implements IStatistic {

    /**
     * Contains threads progress. Represented in %.
     */
    private final HashMap<Long, Double> threadsProgress = new HashMap<Long, Double>();

    /**
     * Creates instance of statistic, with given amount of threads.
     *
     * @param threadsCount amount of threads that will be store their progress
     */
    public Statistic(final int threadsCount) {
        if (threadsCount < 1) {
            throw new IllegalArgumentException("Threads count can be lower than 1");
        }
        for (int i = 0; i <= threadsCount; i++) {
            threadsProgress.put((long) i, 0d);
        }
    }

    /**
     * Returns total progress of application.
     *
     * @return ArrayList with threads progress
     */
    @SuppressWarnings("unchecked")
    @Override
    public synchronized HashMap<Long, Double> getTotalProgress() {
        return (HashMap<Long, Double>) threadsProgress.clone();
    }

    /**
     * Used for save thread progress in %.
     *
     * @param threadId              unique thread id
     * @param progressionPercentage current % of work stage
     */
    @Override
    public synchronized void setThreadProgress(final long threadId, final double progressionPercentage) {
        threadsProgress.put(threadId, progressionPercentage);
    }
}
