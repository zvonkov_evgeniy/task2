package com.sysgears.statistics;


import java.util.HashMap;

/**
 * Describes usage of statistics in multi-threads projects.
 */
public interface IStatistic {

    /**
     * Operates with total progress of iteration. Returns list that contains current
     * progress percentage of each thread.
     *
     * @return list contains progress in percentage of each thread
     */
    public HashMap<Long, Double> getTotalProgress();

    /**
     * Used for save threads progress in %.
     *
     * @param threadId             unique thread id
     * @param progressInPercentage current % of work stage
     */
    public void setThreadProgress(final long threadId, final double progressInPercentage);

}
