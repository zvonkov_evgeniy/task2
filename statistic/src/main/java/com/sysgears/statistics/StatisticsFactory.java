package com.sysgears.statistics;

/**
 * Factory, that manages IStatistic instances.
 */
public class StatisticsFactory {

    /**
     * Creates new instance of IStatistic, with specified amount of threads.
     *
     * @param threadsCount amount of threads that will store their progress
     * @return new instance of IStatistic
     */
    public IStatistic create(final int threadsCount) {
        return new Statistic(threadsCount);
    }

}
