package com.sysgears.io;

import java.io.IOException;

/**
 * Reads expression from given source.
 */
public interface IReaderWriter {

    /**
     * Reads expression from given source.
     *
     * @return expression which need to be calculated
     * @throws IOException if any I/O exceptions found
     */
    public String read() throws IOException;

    /**
     * Sends message to the current source.
     *
     * @param message data message with result of application run
     */
    public void write(final String message) throws IOException;
}
