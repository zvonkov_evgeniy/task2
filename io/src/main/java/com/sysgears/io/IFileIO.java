package com.sysgears.io;

import java.io.File;
import java.io.IOException;

/**
 * Writes and reads data from files.
 */
public interface IFileIO {

    /**
     * Reads portion of data from given file.
     *
     * @param file          file from where to get data
     * @param bytesAmount   sets amount of bytes to read from file
     * @param startPosition position from where to read data
     * @return data read from the file
     * @throws java.io.IOException if exceptions during read from file
     */
    public byte[] read(final File file, final long bytesAmount, final long startPosition) throws IOException;

    /**
     * Writes data to the given file.
     *
     * @param file          file to where write data
     * @param data          data to write to the file
     * @param startPosition position from where to write data
     * @throws java.io.IOException if occurs exceptions during write to file
     */
    public void write(final File file, final byte[] data, final long startPosition) throws IOException;
}
