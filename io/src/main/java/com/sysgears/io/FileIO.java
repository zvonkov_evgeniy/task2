package com.sysgears.io;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Reads and writes data to files.
 */
public class FileIO implements IFileIO {

    /**
     * Reads specified amount of data from file and returns it as byte array.
     *
     * @param file          file from where to read data
     * @param bytesAmount   amount of bytes to be read from file
     * @param startPosition from what position read data in the file
     * @return byte array that contains data read from the file
     * @throws IOException if during closing file stream was thrown an exception
     */
    @Override
    public synchronized byte[] read(final File file,
                                    final long bytesAmount,
                                    final long startPosition) throws IOException {
        byte[] data = new byte[(int) bytesAmount];
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            fileInputStream.getChannel().read(ByteBuffer.wrap(data), startPosition);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        fileInputStream.close();

        return data;
    }

    /**
     * Writes given byte array of data to the file.
     *
     * @param data          data to write to the file
     * @param file          file to where write data
     * @param startPosition position from where to write
     * @throws IOException if during closing file stream was thrown an exception
     */
    @Override
    public synchronized void write(final File file,
                                   final byte[] data,
                                   final long startPosition) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);
        try {
            outputStream.getChannel().write(ByteBuffer.wrap(data), startPosition);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        outputStream.close();
    }
}
