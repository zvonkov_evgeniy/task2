package service;

/**
 * Stores command for application.
 */
public enum Command {

    /**
     * Splits given file into several smaller.
     */
    SPLIT("split"),

    /**
     * Stops the application.
     */
    EXIT("exit"),

    /**
     * Stores info about file splitter.
     */
    HELP("help");

    /**
     * Stores command name.
     */
    private final String name;

    /**
     * Creates instance of Command with specified name.
     *
     * @param name of the command
     */
    Command(final String name) {
        this.name = name;
    }

    /**
     * Returns string which contains name of the current command.
     *
     * @return command name
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Returns command corresponding to the given name.
     *
     * @param name string which contains command name
     * @return command corresponding to given name
     */
    public static Command findByName(final String name) {
        Command command = null;
        for (Command commands : values()) {
            if (commands.name.equals(name.toLowerCase())) {
                command = commands;
            }
        }

        return command;
    }

    /**
     * Creates a list with declared commands and return it as regular expression.
     *
     * @return regular expression with commands names
     */
    public static String commandsList() {
        String commandsList = "";
        for (int i = 0; i < Command.values().length; i++) {
            if (Command.values()[i] == Command.SPLIT) {
                commandsList = commandsList + "|" + Command.SPLIT
                        + " -p \"[\\w\\:\\\\\\w /]+\\w+\\.\\w+\" -s [0-9]*?m|";
            }
            commandsList = commandsList + "|" + Command.values()[i].toString() + "|";
        }

        return commandsList;
    }
}
