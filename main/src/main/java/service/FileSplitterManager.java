package service;


import com.sysgears.io.IReaderWriter;
import com.sysgears.statistics.IStatistic;
import com.sysgears.statistics.StatisticsFactory;
import filesplitter.FileSplitter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Manages files and statistic for FileSplitter.
 */
public class FileSplitterManager {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger("service");

    /**
     * Stores instance of FileSplitter.
     */
    private final FileSplitter splitter;

    /**
     * Stores instance of factory that return new instance of statistic for each iteration.
     */
    private final StatisticsFactory statisticsFactory;

    /**
     * Stores instance of class that used to sends statistic via StatisticWriter.
     */
    private final IReaderWriter ioSource;

    /**
     * Creates instance of FileSplitterManager with given StatisticsFactory, IFileIO and IReaderWriter instances.
     *
     * @param statisticsFactory instance of class that creates new statistic instance for each iteration
     * @param ioSource          instance of class that used to send messages with statistic
     * @param splitter          instance of FileSplitter
     */
    public FileSplitterManager(final StatisticsFactory statisticsFactory,
                               final IReaderWriter ioSource,
                               final FileSplitter splitter) {
        this.statisticsFactory = statisticsFactory;
        this.ioSource = ioSource;
        this.splitter = splitter;
        log.debug("Initialising FileSplitter Manager.");
    }

    /**
     * Launches file splitting process by creating FileSplitter instance and invoking it method split.
     * Sends statistic to the given source with help of StatisticWriter.
     *
     * @param maxFileSize maximum size of each part-file
     * @param fileToSplit file that need to be split into parts
     */
    public void launchSplitter(final long maxFileSize, final File fileToSplit) {
        log.info("Preparing to launch splitter");
        try {
            ArrayList<File> files = createFiles(maxFileSize, fileToSplit);
            IStatistic statistic = statisticsFactory.create(files.size() - 1);
            log.info("Launching statistics sender instance in another thread..");
            Thread thread = new Thread(new StatisticWriter(statistic, ioSource, 1000));
            thread.start();
            log.info("Invoking FileSplitter split method.");
            splitter.split(files, fileToSplit, maxFileSize, statistic);
            while (thread.getState() != Thread.State.TERMINATED) {
                Thread.sleep(50);
            }
        } catch (IOException e) {
            log.error("IOException found.", e);
        } catch (InterruptedException e) {
            log.error("Unexpected Error on thread working with statistic", e);
        }
    }

    /**
     * Returns ArrayList of files ready for save data from one large file.
     *
     * @param maxFileSize indicates counts of needed files
     * @param fileToSplit large file that need to be split
     * @return ArrayList with part-files
     */
    public ArrayList<File> createFiles(final long maxFileSize, final File fileToSplit) throws IOException {
        log.info("Generating part files for saving data..");
        ArrayList<File> files = new ArrayList<File>();
        long totalFilesLength = fileToSplit.length();
        String path = fileToSplit.getAbsolutePath();
        int filesCount = 0;
        while ((filesCount * maxFileSize) <= totalFilesLength) {
            File file = new File(path + "_part " + filesCount);
            if (!file.exists()) {
                log.info("Generate part file with name: " + path + "_part " + filesCount);
                file.createNewFile();
                log.info("Generating file complete.");
            }
            files.add(file);
            filesCount++;
        }
        log.info("All files successfully generated.");

        return files;
    }
}
