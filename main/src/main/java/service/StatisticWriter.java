package service;

import com.sysgears.io.IReaderWriter;
import com.sysgears.statistics.IStatistic;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

/**
 * Writes statistic to the given source.
 */
public class StatisticWriter implements Runnable {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(StatisticWriter.class);

    /**
     * Stores instance of statistic class.
     */
    private final IStatistic statistic;

    /**
     * Stores instance of class that used for send messages to source.
     */
    private final IReaderWriter ioSource;

    /**
     * Stores update delay for sending statistic.
     */
    private final int updateDelay;

    /**
     * Creates a new instance of StatisticWriter with specified instances of statistic and output source.
     *
     * @param statistic   instance of class that gather statistic
     * @param ioSource    instance of class that used for sending messages with statistic
     * @param updateDelay period between sending messages with statistic, can be only > 0
     */
    public StatisticWriter(final IStatistic statistic, final IReaderWriter ioSource, final int updateDelay) {
        log.debug("Initialising new instance of StatisticWriter.");
        if (updateDelay < 0) {
            log.fatal("Update delay was setter < 0.");
            throw new IllegalArgumentException("Update delay can be lower than 0!");
        }
        this.statistic = statistic;
        this.ioSource = ioSource;
        this.updateDelay = updateDelay;
    }

    /**
     * Sends statistic to the given source with updateDelay period.
     *
     * @throws java.io.IOException if during send messages to the source was thrown an exception
     */
    public synchronized void sendStatistics(final int updateDelay) throws IOException {
        log.info("Starts sending statistic to the source.");
        try {
            double totalProgress = 0d;
            double threadProgress;
            Collection<Double> threadsProgress;
            while (totalProgress != 100) {
                this.wait(updateDelay);
                totalProgress = 0d;
                threadsProgress = statistic.getTotalProgress().values();
                Iterator<Double> iterator = threadsProgress.iterator();
                int i = 0;
                while (iterator.hasNext()) {
                    threadProgress = iterator.next();
                    totalProgress = totalProgress + threadProgress;
                    ioSource.write("Thread " + (i++) + " progress: " + threadProgress + "%\n");
                }
                totalProgress = totalProgress / threadsProgress.size();
                ioSource.write("Total progress: " + totalProgress + "%\n");
                ioSource.write("\n");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Runs process of sending statistic.
     */
    @Override
    public void run() {
        log.info("Running StatisticWriter in a thread: " + Thread.currentThread().getName());
        try {
            sendStatistics(updateDelay);
        } catch (IOException e) {
            log.error("Error during sending statistic.", e);
        } catch (Throwable t) {
            log.error("Error executing sendStatistics method.", t);
        }
    }
}
