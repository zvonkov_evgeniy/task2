package service;


import com.sysgears.io.IReaderWriter;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Starts application and control it.
 */
public class FileSplitterService implements Runnable {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger("service");

    /**
     * Stores instance of FileManager, that prepare FileSplitter for work.
     */
    private final FileSplitterManager fileManager;

    /**
     * Stores instance of class that used for write or read data from files.
     */
    private final IReaderWriter ioSource;

    /**
     * Creates instance with given parameters.
     *
     * @param fileManager instance of class that prepare FileSplitter for continue
     * @param ioSource    instance of class that used for write or read data from files
     */
    public FileSplitterService(final FileSplitterManager fileManager,
                               final IReaderWriter ioSource) {
        this.fileManager = fileManager;
        this.ioSource = ioSource;
    }

    /**
     * Manages application using the following instances of classes.
     */
    @Override
    public void run() {
        try {
            String command;
            File fileToSplit;
            long maxFileSize;
            ioSource.write("You are using FileSplitter. To see information about this program type \"help>\"" + "\n");
            loop:
            while (true) {
                ioSource.write("Type your command and press <Enter>" + "\n");
                log.info("Obtaining command..");
                command = ioSource.read();
                log.info("Getting command: " + command);
                ioSource.write("You type: " + command + "\n");
                if (command.matches(Command.commandsList())) {
                    String[] splitCommand = command.split(" ");
                    String forSwitch;
                    if (!splitCommand[0].equals(null)) {
                        forSwitch = splitCommand[0];
                    } else {
                        forSwitch = command;
                    }
                    switch (Command.findByName(forSwitch)) {
                        case SPLIT:
                            String path = splitCommand[2].split("\"")[1];
                            fileToSplit = new File(path);
                            if (!fileToSplit.exists()) {
                                log.error("File wasn't found.");
                                ioSource.write("File " + path + " doesn't exist." + "\n");
                            } else {
                                maxFileSize = Long.parseLong(splitCommand[4].split("m")[0]);
                                maxFileSize = maxFileSize * 1000 * 1000;
                                long curTime = System.currentTimeMillis();
                                fileManager.launchSplitter(maxFileSize, fileToSplit);
                                ioSource.write("Complete. Time spend = " +
                                        ((System.currentTimeMillis() - curTime) / 1000) + "sec" + "\n");
                                log.info("FileSplitter is ready for next iteration.");
                            }
                            break;
                        case HELP:
                            ioSource.write("For split file enter following command:" + "\n");
                            ioSource.write("split -p \"path to file\" -s 0-9m" + "\n");
                            ioSource.write("where 0-9 size of part files in megabytes " +
                                    "(size can only int value greater than 0)." + "\n");
                            ioSource.write("To close application properly type \"exit\"." + "\n");
                            break;
                        case EXIT:
                            ioSource.write("Thank you for using FileSplitter." + "\n");
                            log.info("Closing application.");
                            break loop;
                        default:
                            ioSource.write("Unknown command. To get help type \"help\"" + "\n");
                    }
                }
            }
            log.debug("FileSplitter service is closed.");
        } catch (Throwable throwable) {
            log.fatal("Error executing FileSplitterService.", throwable);
        }
    }
}
