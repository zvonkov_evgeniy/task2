package filesplitter;


import org.apache.log4j.Logger;

/**
 * Class that provides calculation of progress and write/read points for file saver.
 */
public class BufferIterator {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger("splitter");

    /**
     * Stores start point in large file.
     */
    private final long startPoint;

    /**
     * Stores end point in large file.
     */
    private final long endPoint;

    /**
     * Stores current point in part-file.
     */
    private long currentPoint;

    /**
     * Stores current amount of bytes need to be read/write.
     */
    private long bufferSize;

    /**
     * Creates an instance of BufferIterator and sets start and end points.
     *
     * @param startPoint from where to start writing in large file
     * @param endPoint   point to where read in large file
     */
    public BufferIterator(final long startPoint, final long endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        currentPoint = startPoint;
        log.debug("Initialising new BufferIterator with parameters: startPoint " + startPoint +
                "; endPoint: " + endPoint +
                "; currentPoint: " + currentPoint);
    }

    /**
     * Returns true if the iteration has more elements.
     *
     * @return true if the iteration has more elements.
     */
    public boolean hasNext() {
        return ((endPoint - currentPoint) > 0);
    }

    /**
     * Returns next element - new buffer size for iteration.
     *
     * @return buffer size
     */
    public long next() {
        bufferSize = 8 * 1024;
        if ((endPoint - currentPoint) < bufferSize) {
            bufferSize = endPoint - currentPoint;
        }

        return bufferSize;
    }

    /**
     * Returns progress of work.
     *
     * @return progress of work from 0 to 100
     */
    public double progress() {
        return ((currentPoint - startPoint) * 100 / (endPoint - startPoint));
    }

    /**
     * Returns current point in large file.
     *
     * @return current point in large file
     */
    public long getCurrentPoint() {
        long oldPoint = currentPoint;
        currentPoint = currentPoint + bufferSize;

        return oldPoint;
    }

    /**
     * Returns current point in part-file.
     *
     * @return current point in part-file
     */
    public long getWritePoint() {
        return (currentPoint - startPoint);
    }
}
