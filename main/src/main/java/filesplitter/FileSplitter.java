package filesplitter;


import com.sysgears.io.IFileIO;
import com.sysgears.statistics.IStatistic;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Splits one large file into several parts. FileSplitter uses multi-thread split system.
 * At once it can use only 20 threads. Threads that waiting for start running sets their
 * statistic at 0% for all waiting period.
 */
public class FileSplitter implements IFileSplitter {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger("splitter");

    /**
     * Stores instance of class that used for write or read data from files.
     */
    private final IFileIO fileIO;

    /**
     * Stores ThreadPoolExecutor that operates with
     */
    private final ThreadPoolExecutor threadPoolExecutor;

    /**
     * Stores instance of FileSaverFactory that used to creates instances of FileSaver.
     */
    private final FileSaverFactory saverFactory;

    /**
     * Creates instance of FileSplitter and sets it parameters for splitting.
     *
     * @param threadName   single thread name
     * @param fileIO       instance of class that used for write or read data from files
     * @param saverFactory instance of factory that creates FileSaver instances
     */
    public FileSplitter(final String threadName,
                        final IFileIO fileIO,
                        final FileSaverFactory saverFactory) {
        this.fileIO = fileIO;
        final SynchronousQueue<Runnable> queue = new SynchronousQueue<Runnable>();
        threadPoolExecutor = new ThreadPoolExecutor(
                0,
                20,
                100L,
                TimeUnit.MILLISECONDS,
                queue,
                new NamedThreadFactory(threadName),
                new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        try {
                            threadPoolExecutor.getQueue().put(r);
                        } catch (InterruptedException ie) {
                            Thread t = new Thread(r);
                            t.start();
                            t.interrupt();
                        }
                    }
                });
        this.saverFactory = saverFactory;
        log.debug("Initialising File Splitter. Incoming parameters: threadName " + threadName);
    }

    /**
     * Controls process of file splitting.
     *
     * @param files       list of part-files
     * @param fileToSplit file that need to be split
     * @param maxFileSize maximum length of part file
     * @param statistic   for statistic gathering
     */
    @Override
    public void split(final ArrayList<File> files,
                      final File fileToSplit,
                      final long maxFileSize,
                      final IStatistic statistic) {
        log.info("File: " + fileToSplit.getName() + " will be split into parts.");
        for (int i = 0; i < files.size(); i++) {
            log.info("Splitting iteration no: " + i);
            long startPosition = i * maxFileSize;
            long targetPosition = (i + 1) * maxFileSize;
            if (targetPosition > fileToSplit.length()) {
                targetPosition = fileToSplit.length();
            }
            log.info("Current parameters of iteration: part-file name: " + files.get(i).getName() +
                    "; start position: " + startPosition +
                    ": target position: " + targetPosition);
            threadPoolExecutor.execute(saverFactory.create(startPosition,
                    targetPosition,
                    fileToSplit,
                    files.get(i),
                    statistic,
                    fileIO,
                    i));
        }
    }
}