package filesplitter;

import com.sysgears.io.IFileIO;
import com.sysgears.statistics.IStatistic;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Saves data to one file from another.
 */
public class FileSaver implements Runnable {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger("splitter");

    /**
     * Stores file from where to read.
     */
    private final File fileToSplit;

    /**
     * Stores file where to write.
     */
    private final File partFile;

    /**
     * Stores instance of statistic,
     */
    private final IStatistic statistic;

    /**
     * Stores instance of class that used for write or read data from files
     */
    private final IFileIO fileIO;

    /**
     * Stores statistics index of current instance.
     */
    private final int indexOfStatisticCell;

    /**
     * Stores BufferIterator instance for calculating progress and points.
     */
    private final BufferIterator iterator;

    /**
     * Creates FileSaver with given instances.
     *
     * @param fileToSplit          file where to read
     * @param partFile             file where to write
     * @param statistic            instance of statistic
     * @param fileIO               instance of class that used for write or read data from files
     * @param indexOfStatisticCell index of current class instance in statistic
     */
    public FileSaver(final File fileToSplit,
                     final File partFile,
                     final IStatistic statistic,
                     final IFileIO fileIO,
                     final int indexOfStatisticCell,
                     final BufferIterator iterator) {
        this.fileToSplit = fileToSplit;
        this.partFile = partFile;
        this.statistic = statistic;
        this.fileIO = fileIO;
        this.indexOfStatisticCell = indexOfStatisticCell;
        this.iterator = iterator;
        log.debug("Initialising FileSaver. Incoming parameters: fileToSplit: " + fileToSplit.getName() +
                "; partFile: " + partFile.getName() +
                "; indexOfStatisticCell " + indexOfStatisticCell);
    }

    /**
     * Reads data from the source file and writes it to another file.
     */
    @Override
    public void run() {
        try {
            log.info("Starts to save file: " + partFile.getName());
            while (iterator.hasNext()) {
                long writePoint = iterator.getWritePoint();
                long bufferSize = iterator.next();
                long currentPoint = iterator.getCurrentPoint();
                byte[] data = fileIO.read(fileToSplit, bufferSize, currentPoint);
                fileIO.write(partFile, data, writePoint);
                statistic.setThreadProgress(indexOfStatisticCell, iterator.progress());
            }
            log.info("Save file was completed successfully.");
        } catch (IOException exception) {
            log.error("IO exception during saving file. " +
                    "Current parameters: current point in part-file:" + iterator.getWritePoint() +
                    "; part-file size: " + partFile.length() +
                    "; current point in fileToSplit: " + iterator.getCurrentPoint() +
                    "; fileToSplit size: " + fileToSplit.length(), exception);
        } catch (Throwable t) {
            log.fatal("Error executing" + Thread.currentThread().getName() + " task. " +
                    "Current parameters: current point in part-file:" + iterator.getWritePoint() +
                    "; part-file size: " + partFile.length() +
                    "; current point in fileToSplit: " + iterator.getCurrentPoint() +
                    "; fileToSplit size: " + fileToSplit.length(), t);
        }
    }
}
