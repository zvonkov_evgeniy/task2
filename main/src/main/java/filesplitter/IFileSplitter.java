package filesplitter;


import com.sysgears.statistics.IStatistic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Describes rules for file splitter.
 */
public interface IFileSplitter {

    /**
     * Splits one file into several parts.
     *
     * @param files       list of part-files
     * @param fileToSplit file that need to be split
     * @param maxFileSize maximum length of part file
     * @param statistic   for statistic gathering
     * @throws java.io.IOException if occurs exceptions during read or write from file
     */
    public void split(ArrayList<File> files,
                      File fileToSplit,
                      long maxFileSize,
                      IStatistic statistic) throws IOException;
}
