package filesplitter;


import org.apache.log4j.Logger;

import java.util.concurrent.ThreadFactory;

/**
 * Manages threads for FileSplitter.
 */
public class NamedThreadFactory implements ThreadFactory {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(NamedThreadFactory.class);

    /**
     * Stores current thread name.
     */
    final String name;

    /**
     * Creates new named thread factory with given name.
     *
     * @param name of new named thread factory
     */
    public NamedThreadFactory(final String name) {
        log.debug("Initialising Named Thread Factory, with name: " + name);
        this.name = name;
    }

    /**
     * Constructs a new {@code Thread}.  Implementations may also initialize
     * priority, name, daemon status, {@code ThreadGroup}, etc.
     *
     * @param r a runnable to be executed by new thread instance
     * @return constructed thread with specified name, or {@code null} if the request to
     *         create a thread is rejected
     */
    @SuppressWarnings("NullableProblems")
    @Override
    public Thread newThread(Runnable r) {
        log.debug("Initialising new Thread. For: " + r.getClass().getName() + "; thread name: " + name);
        return new Thread(r, name);
    }
}
