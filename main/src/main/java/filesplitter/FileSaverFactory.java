package filesplitter;


import com.sysgears.io.IFileIO;
import com.sysgears.statistics.IStatistic;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Returns new instances of FileSaver.
 */
public class FileSaverFactory {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(FileSaverFactory.class);

    /**
     * Creates new instance of FileSaverFactory.
     */
    public FileSaverFactory(){
        log.debug("Initialising instance of FileSaverFactory.");
    }

    /**
     * Creates new instance of FileSaver and BufferIterator for it.
     *
     * @param startPosition        in large file
     * @param endPosition          in large file
     * @param fileToSplit          large file need to be split
     * @param partFile             file for saving data
     * @param statistic            instance of statistic to store progress
     * @param fileIO               instance of class that manages write and read process from files
     * @param indexOfStatisticCell current cell in statistic instance
     * @return new instance of FileSaver
     */
    public FileSaver create(final long startPosition,
                            final long endPosition,
                            final File fileToSplit,
                            final File partFile,
                            final IStatistic statistic,
                            final IFileIO fileIO,
                            final int indexOfStatisticCell) {
        return new FileSaver(fileToSplit,
                partFile,
                statistic,
                fileIO,
                indexOfStatisticCell,
                new BufferIterator(startPosition, endPosition));
    }
}
