import com.sysgears.io.FileIO;
import com.sysgears.io.IFileIO;
import com.sysgears.io.IOController;
import com.sysgears.io.IReaderWriter;
import com.sysgears.statistics.StatisticsFactory;
import filesplitter.FileSaverFactory;
import filesplitter.FileSplitter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import service.FileSplitterManager;
import service.FileSplitterService;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Creates all necessary instances of classes. Calls FileSplitterService service method.
 */
public class Main {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(Main.class);

    /**
     * Creates all instance of class needed to continue.
     * After that invokes FileSplitterService service method.
     *
     * @param args parameters for launching application
     */
    public static void main(String[] args) throws IOException {
        PropertyConfigurator.configure("log4j.properties");
        log.info("Starting main method. Creating all final instances.");

        // Controls process of read and write data to files.
        final IFileIO fileIO = new FileIO();

        // Controls input and output streams for communication with user.
        final IReaderWriter ioSource = new IOController(new PrintStream(System.out),
                new BufferedInputStream(System.in));

        // Manages statistics instances.
        final StatisticsFactory statisticFactory = new StatisticsFactory();

        // Factory that creates FileSaver instances.
        final FileSaverFactory saverFactory = new FileSaverFactory();

        // Splits file into part-files.
        final FileSplitter splitter = new FileSplitter("Splitter", fileIO, saverFactory);

        // Manages with files for FileSplitter.
        final FileSplitterManager fileManager = new FileSplitterManager(statisticFactory,
                ioSource,
                splitter);

        Thread thread = new Thread(new FileSplitterService(fileManager,
                ioSource));
        thread.start();
    }
}                       //split -p "/home/eugene/Downloads/ideaIU12.gz" -s 10m
//split -p "/home/eugene/Downloads/split.zip" -s 10m
