package filesplitter;


import com.sysgears.io.FileIO;
import com.sysgears.io.IFileIO;
import com.sysgears.io.IOController;
import com.sysgears.io.IReaderWriter;
import com.sysgears.statistics.StatisticsFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import service.FileSplitterManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Test.
 */
public class UTestFileSaver {

    final IFileIO fileIO = new FileIO();

    final IReaderWriter ioSource = new IOController(new PrintStream(System.out),
            new BufferedInputStream(System.in));

    final StatisticsFactory statisticFactory = new StatisticsFactory();

    final FileSaverFactory saverFactory = new FileSaverFactory();

    final FileSplitter splitter = new FileSplitter("Splitter", fileIO, saverFactory);

    final FileSplitterManager fileManager = new FileSplitterManager(statisticFactory,
            ioSource,
            splitter);

    /**
     * Tests BufferIterator hasNext() method for correct work.
     */
    @Test
    public void iteratorNext() {
        System.out.println("iteratorNext()");
        BufferIterator iterator = new BufferIterator(0, 0);
        Assert.assertEquals(iterator.hasNext(), false);
        iterator = new BufferIterator(100, 200);
        Assert.assertEquals(iterator.hasNext(), true);
        iterator = new BufferIterator(200, 100);
        Assert.assertEquals(iterator.hasNext(), false);
    }

    /**
     * Tests BufferIterator for correct end point.
     *
     * @param fullFilePath path to file
     * @throws IOException if file not exist
     */
    @Test
    @Parameters(value = "fullFilePath")
    public void fullIteratorTest(String fullFilePath) throws IOException {
        File fileToSplit = new File(fullFilePath);
        final ArrayList<File> partFiles = fileManager.createFiles(20 * 1000 * 1000, fileToSplit);
        BufferIterator iterator;
        for (int i = 0; i < partFiles.size(); i++) {
            long startPoint = i * 20 * 1000 * 1000;
            long endPoint = (i + 1) * 20 * 1000 * 1000;
            if (((i + 1) * 20 * 1000 * 1000) > fileToSplit.length()) {
                endPoint = fileToSplit.length();
            }
            iterator = new BufferIterator(startPoint, endPoint);
            while (iterator.hasNext()) {
                iterator.next();
                iterator.getCurrentPoint();
            }
            Assert.assertEquals(endPoint, iterator.getCurrentPoint());
        }
    }

    /**
     * Test full work of splitting process.
     *
     * @param fullFilePath path to file to split
     * @throws IOException if io exception found
     */
    @Test
    @Parameters(value="fullFilePath")
    public void testSplitter(String fullFilePath) throws IOException {
        File fileToSplit = new File(fullFilePath);
        final long largeFileSize = fileToSplit.length();
        long partFilesSize = 0;
        final ArrayList<File> partFiles = fileManager.createFiles(20 * 1000 * 1000, fileToSplit);
        fileManager.launchSplitter(20 * 1000 * 1000, fileToSplit);
        for (int i = 0; i < partFiles.size(); i++) {
            partFilesSize += partFiles.get(i).length();
        }
        Assert.assertEquals(partFilesSize, largeFileSize);
    }
}
